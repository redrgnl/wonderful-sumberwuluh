<?php
namespace App;

use Illuminate\Support\Facades\DB;

class Helper {

    public static function setting($field) {
        $query = DB::table('setting')->select($field)->first();
        return $query->$field;
    }

    public static function helpWisata($limit) {
        $query = DB::table('wisata')->where('wis_status', 1)->orderBy('created_at', 'desc')->limit($limit)->get();
        $output = '';
        foreach ($query as $ds) {
            $output .= "<li class='truncate-text'><a href='/destination/".str_replace(' ', '-', strtolower($ds->wis_nama))."'>".ucwords($ds->wis_nama)."</a></li>";
        }

        return $output;
    }

    public static function slider() {
        $query = DB::table('slider')->where('sli_status', 1)->get();
        $slider = '';
        if($query->count() == 0) {
            return $slider;
        } else {
            foreach ($query as $q) {
                $slider .= "
                <li style='background-image: url(".$q->sli_foto.");'>
                    <div class='overlay'></div>
                    <div class='container-fluids'>
                        <div class='row'>
                            <div class='col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text'>
                                <div class='slider-text-inner text-center'>
                                    <h2>".$q->sli_subtitle."</h2>
                                    <h1>".$q->sli_title."</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>";
            }

            return $slider;
        }
    }
}
