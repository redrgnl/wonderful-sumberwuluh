<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function dashboard() {
        Session::put('admMnActive', 'dashboard');
        $data = [
            'breadcrumb' => 'Dashboard',
            'wisata' => DB::table('wisata')->where('wis_status', 1)->count(),
            'wisatawan' => DB::table('wisatawan')->where('wita_status', 1)->count(),
            'slider' => DB::table('slider')->where('sli_status', 1)->count(),
        ];
        return view('admin._content.dashboard', $data);
    }

    public function wisata() {
        Session::put('admMnActive', 'wisata');
        $data = [
            'breadcrumb' => 'Wisata',
            'wisata' => DB::table('wisata')->join('admin', 'admin.adm_id', '=', 'wisata.adm_id')->select('wisata.*', 'admin.adm_nama')->where('wisata.wis_status', '1')->get()
        ];
        return view('admin._content.wisata', $data);
    }

    public function addWisata(Request $insert) {
        DB::table('wisata')->insert([
            'wis_nama' => $insert->inpnama,
            'wis_desk' => $insert->inpdesk,
            'wis_foto' => $insert->inpfoto,
            'adm_id' => Session::get('admId'),
        ]);

        return redirect('/admin/wisata');
    }

    public function editWisata(Request $update) {
        DB::table('wisata')->where('wis_id', $update->edid)->update([
            'wis_nama' => $update->ednama,
            'wis_desk' => $update->eddesk,
            'wis_foto' => $update->edfoto,
            'adm_id' => Session::get('admId'),
        ]);

        return redirect('/admin/wisata');
    }

    public function deleteWisata(Request $delete) {
        DB::table('wisata')->where('wis_id', $delete->delid)->update([
            'wis_status' => 0,
            'adm_id' => Session::get('admId'),
        ]);

        return redirect('/admin/wisata');
    }

    public function testimoni() {
        Session::put('admMnActive', 'testimoni');
        $data = [
            'breadcrumb' => 'Testimoni',
            'testimoni' => DB::table('wisatawan')->join('admin', 'admin.adm_id', '=', 'wisatawan.adm_id')->join('wisata', 'wisata.wis_id', '=', 'wisatawan.wis_id')->select('wisatawan.*', 'admin.adm_nama', 'wisata.wis_nama')->where('wisatawan.wita_status', '1')->get(),
            'wisata' => DB::table('wisata')->where('wis_status', 1)->get()
        ];
        return view('admin._content.testimoni', $data);
    }

    public function addTestimoni(Request $insert) {
        DB::table('wisatawan')->insert([
            'wita_nama' => $insert->inpnama,
            'wita_desk' => $insert->inpdesk,
            'wita_foto' => $insert->inpfoto,
            'wis_id' => $insert->inpwisata,
            'adm_id' => Session::get('admId'),
        ]);

        return redirect('/admin/testimoni');
    }

    public function editTestimoni(Request $update) {
        DB::table('wisatawan')->where('wita_id', $update->edid)->update([
            'wita_nama' => $update->ednama,
            'wita_desk' => $update->eddesk,
            'wita_foto' => $update->edfoto,
            'wis_id' => $update->edwisata,
            'adm_id' => Session::get('admId'),
        ]);

        return redirect('/admin/testimoni');
    }

    public function deleteTestimoni(Request $delete) {
        DB::table('wisatawan')->where('wita_id', $delete->delid)->update([
            'wita_status' => 0,
            'adm_id' => Session::get('admId'),
        ]);

        return redirect('/admin/testimoni');
    }


    public function pengaturan() {
        Session::put('admMnActive', 'pengaturan');
        $data = [
            'breadcrumb' => 'Pengaturan',
            'setting' => DB::table('setting')->join('admin', 'admin.adm_id', '=', 'setting.adm_id')->select('setting.*', 'admin.adm_nama')->first()
        ];
        return view('admin._content.pengaturan', $data);
    }

    public function savePengaturan(Request $update) {
        DB::table('setting')->where('set_id', $update->inpid)->update([
            'set_foto' => $update->inpfoto,
            'set_title' => $update->inpjudul,
            'set_desk' => $update->inpdesk,
            'set_intro_title' => $update->inpintjudul,
            'set_intro_subtitle' => $update->inpintdesk,
            'set_intro_thumbnail' => $update->inpintthumb,
            'set_intro_video' => $update->inpintvid,
            'set_intro_foto' => $update->inpintbg,
            'set_sewa_bg' => $update->inpswbg,
            'set_sewa_foto' => $update->inpswft,
            'set_sewa_title' => $update->inpswjd,
            'set_sewa_desk' => $update->inpswdesk,
            'set_footer_foto' => $update->inpfotbg,
            'set_footer_desk' => $update->inpfotdesk,
            'set_fb' => $update->inpfb,
            'set_ig' => $update->inpig,
            'set_twitter' => $update->inptw,
            'set_nama' => $update->inpnmcp,
            'set_alamat' => $update->inpalcp,
            'set_telp' => $update->inptpcp,
            'set_wa' => $update->inpwacp,
            'adm_id' => Session::get('admId'),
        ]);
        return redirect('/admin/pengaturan');
    }

    public function slider() {
        Session::put('admMnActive', 'slider');
        $data = [
            'breadcrumb' => 'Slider',
            'slider' => DB::table('slider')->join('admin', 'admin.adm_id', '=', 'slider.adm_id')->select('slider.*', 'admin.adm_nama')->where('slider.sli_status', '1')->get()

        ];
        return view('admin._content.slider', $data);
    }

    public function addSlider(Request $insert) {
        DB::table('slider')->insert([
            'sli_title' => $insert->inptitle,
            'sli_subtitle' => $insert->inpsubtitle,
            'sli_foto' => $insert->inpfoto,
            'adm_id' => Session::get('admId'),
        ]);

        return redirect('/admin/slider');
    }

    public function editSlider(Request $update) {
        DB::table('slider')->where('sli_id', $update->edid)->update([
            'sli_title' => $update->edtitle,
            'sli_subtitle' => $update->edsubtitle,
            'sli_foto' => $update->edfoto,
            'adm_id' => Session::get('admId'),
        ]);

        return redirect('/admin/slider');
    }

    public function deleteSlider(Request $delete) {
        DB::table('slider')->where('sli_id', $delete->delid)->update([
            'sli_status' => 0,
            'adm_id' => Session::get('admId'),
        ]);

        return redirect('/admin/slider');
    }

    public function pemesanan() {
        Session::put('admMnActive', 'pemesanan');
        $data = [
            'breadcrumb' => 'Pemesanan',
            'pemesanan' => DB::table('det_booking')->orderBy('created_at', 'desc')->get(),
            'paket' => DB::table('paket')->get(),
            'tenda' => DB::table('tenda')->get(),
        ];
        return view('admin._content.pemesanan', $data);
    }

    public function pemesananKonfirmasi($id) {
        DB::table('det_booking')->where('book_id', $id)->update([
            'book_status' => 1
        ]);

        return redirect('/admin/pemesanan');
    }

    public function pemesananBatal($id) {
        DB::table('det_booking')->where('book_id', $id)->update([
            'book_status' => 0
        ]);

        return redirect('/admin/pemesanan');
    }

    public function pemesananKembali($id) {
        DB::table('det_booking')->where('book_id', $id)->update([
            'book_kembali' => 1
        ]);

        return redirect('/admin/pemesanan');
    }

    public function pemesananBelumKembali($id) {
        DB::table('det_booking')->where('book_id', $id)->update([
            'book_kembali' => 0
        ]);

        return redirect('/admin/pemesanan');
    }

    public function akun() {
        Session::put('admMnActive', 'akun');
        $data = [
            'breadcrumb' => 'Akun',
            'akun' => DB::table('admin')->where('adm_id', 1)->first()
        ];
        return view('admin._content.akun', $data);
    }

    public function saveAkun(Request $update) {
        DB::table('admin')->where('adm_id', $update->inpid)->update([
            'adm_nama' => $update->inpnama,
            'adm_pass' => hash('ripemd160', $update->inppass)
        ]);
        return redirect('/admin/akun');
    }

    public function login()
    {
        if (Session::get('login') != NULL) {
            return redirect('/admin/dashboard');
        } else {
            return view('admin.login');
        }
    }

    public function post_login(Request $insert) {
        $query = DB::table('admin')->where('adm_nama', $insert->inpnama)->where('adm_pass', hash('ripemd160', $insert->inppass))->where('adm_status', 1)->first();
        if(!empty($query)) {
            Session::put('admId', $query->adm_id);
            Session::put('admName', $query->adm_nama);
            Session::put('login', TRUE);
            return redirect('/admin/dashboard');
        } else {
            return redirect('/admin/login');
        }
    }

    public function post_logout() {
        Session::flush();
        return redirect('/admin/login');
    }
}
