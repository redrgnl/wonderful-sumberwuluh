<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PublicController extends Controller
{
    public function index() {
        Session::put('mnActive', 'beranda');
        $data = [
            'setting' => DB::table('setting')->first(),
            'destinasi' => DB::table('wisata')->where('wis_status', '1')->orderBy('created_at', 'desc')->limit(8)->get(),
            'destinasi_side' => DB::table('wisata')->where('wis_status', '1')->orderBy('created_at', 'desc')->limit(5)->get(),
            'wisatawan' => DB::table('wisatawan')->join('wisata', 'wisata.wis_id', '=', 'wisatawan.wis_id')->select('wisata.wis_nama', 'wisatawan.*')->where('wisatawan.wita_status', 1)->orderBy('created_at', 'desc')->limit(10)->get(),
        ];

        return view('public._content.dashboard', $data);
    }

    public function destination() {
        Session::put('mnActive', 'destinasi');
        $data = [
            'destinasi' => DB::table('wisata')->where('wis_status', '1')->orderBy('created_at', 'desc')->get(),
        ];

        return view('public._content.destination', $data);
    }

    public function destinationDetail($nama) {
        Session::put('mnActive', 'destinasi');
        $nama = str_replace('-', ' ', $nama);
        $query = DB::table('wisata')->join('admin', 'admin.adm_id', '=', 'wisata.adm_id')->select('admin.adm_nama', 'wisata.*')->where('wisata.wis_status', '1')->where('wisata.wis_nama', 'like', '%'.$nama.'%')->first();
        if(!$query) {
            return redirect('/');
        }
        $data = [
            'destinationDetail' => $query,
            'destinationName' => $query->wis_nama,
            'bottDestination' => DB::table('wisata')->where('wis_status', '1')->orderBy('created_at', 'desc')->limit(6)->get(),
            'bottWisatawan' => DB::table('wisatawan')->join('wisata', 'wisata.wis_id', '=', 'wisatawan.wis_id')->select('wisatawan.*', 'wisata.wis_nama')->where('wisatawan.wita_status', '1')->where('wisatawan.wis_id', $query->wis_id)->orderBy('wisatawan.created_at', 'desc')->get()
        ];

        return view('public._content.destination_detail', $data);
    }

    public function camp() {
        Session::put('mnActive', 'destinasi');
        $data = [
            'tenda' => DB::table('tenda')->where('tent_status', '1')->orderBy('created_at', 'desc')->get(),
            'paket' => DB::table('paket')->where('paket_status', '1')->orderBy('created_at', 'desc')->get(),
        ];

        return view('public._content.camp', $data);
    }

    public function campBook() {
        Session::put('mnActive', 'destinasi');
        $data = [
            'tenda' => DB::table('tenda')->where('tent_status', '1')->where('tent_sedia', '1')->orderBy('created_at', 'desc')->get(),
        ];

        return view('public._content.camp_book', $data);
    }

    public function campBookSave(Request $request) {
        $id = "BK-".date('Ymd')."-".date('His');
        if($request->inppaket == '') {
            $paket = 0;
            $tools = implode(',', $request->checktools);
        } else {
            $paket = $request->inppaket;
            $tools = $request->inppakettools;
        }
        DB::table('det_booking')->insert([
            'book_id' => $id,
            'book_nama' => $request->inpnama,
            'book_nik' => $request->inpnik,
            'book_alamat' => $request->inpalamat,
            'book_telp' => $request->inptelp,
            'book_email' => $request->inpemail,
            'book_start' => $request->inpstart,
            'book_end' => $request->inpend,
            'book_tools' => $tools,
            'book_paket' => $paket,
        ]);

        Session::put('bookSession', $id);
        if($paket != 0) {
            return redirect('/camp-tools/paket-detail');
        } else {
            return redirect('/camp-tools/detail');
        }
    }

    public function campBookCheck($id) {
        $query = DB::table('det_booking')->where('book_id', $id)->first();
        if($query) {
            Session::put('bookSession', $id);
            if ($query->book_paket != 0) {
                return redirect('/camp-tools/paket-detail');
            } else {
                return redirect('/camp-tools/detail');
            }
        } else {
            return redirect('/camp-tools/book');
        }
    }

    public function campBookDetail() {
        Session::put('mnActive', 'destinasi');
        if (Session::has('bookSession')) {
            $data = [
                'tenda' => DB::table('tenda')->where('tent_status', '1')->where('tent_sedia', '1')->orderBy('created_at', 'desc')->get(),
                'bookDetail' => DB::table('det_booking')->where('book_id', Session::get('bookSession'))->first(),
            ];

            return view('public._content.camp_book_detail', $data);
        } else {
            return redirect('/camp-tools/book');
        }
    }

    public function campPaketBookDetail() {
        Session::put('mnActive', 'destinasi');
        if (Session::has('bookSession')) {
            $detail = DB::table('det_booking')->where('book_id', Session::get('bookSession'))->first();
            $data = [
                'tenda' => DB::table('tenda')->where('tent_status', '1')->where('tent_sedia', '1')->orderBy('created_at', 'desc')->get(),
                'paket' => DB::table('paket')->where('paket_id', $detail->book_paket)->first(),
                'bookDetail' => $detail,
            ];

            return view('public._content.camp_paket_book_detail', $data);
        } else {
            return redirect('/camp-tools/book');
        }
    }

    public function campPaketDetail($nama) {
        Session::put('mnActive', 'destinasi');
        $nama = str_replace('-', ' ', $nama);
        $query = DB::table('paket')->join('admin', 'admin.adm_id', '=', 'paket.adm_id')->select('admin.adm_nama', 'paket.*')->where('paket.paket_status', '1')->where('paket.paket_nama', 'like', '%'.$nama.'%')->first();
        if(!$query) {
            return redirect('/');
        }
        $data = [
            'paketDetail' => $query,
            'paketName' => $query->paket_nama,
            'bottPaket' => DB::table('paket')->where('paket_status', '1')->orderBy('created_at', 'desc')->get(),
            'bottPeralatan' => DB::table('tenda')->where('tent_status', '1')->orderBy('created_at', 'desc')->get(),
        ];

        return view('public._content.camp_paket_detail', $data);
    }

    public function campPaketId($id) {
        $query = DB::table('paket')->where('paket_id', $id)->get();
        if($query->count() > 0) {
            Session::put('paketBook', $id);
            return redirect('/camp-tools/paket-book');
        } else {
            return redirect('/camp-tools');
        }
    }

    public function campPaketBook() {
        Session::put('mnActive', 'destinasi');
        if (Session::has('paketBook')) {
            $data = [
                'paketDetail' => DB::table('paket')->where('paket_status', '1')->where('paket_id', Session::get('paketBook'))->first(),
                'tenda' => DB::table('tenda')->where('tent_status', '1')->orderBy('created_at', 'desc')->get(),
            ];

            return view('public._content.camp_paket_book', $data);
        } else {
            return redirect()->back();
        }
    }
}
