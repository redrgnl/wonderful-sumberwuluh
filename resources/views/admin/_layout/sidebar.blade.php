<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://redrgnl.github.io/asset/wonderfulsumberwuluh/admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ ucwords(Session::get('admName')) }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">Menu</li>
          <li class="nav-item">
            <a href="/admin/dashboard" class="nav-link <?= (Session::get('admMnActive') == 'dashboard') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-tv"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/wisata" class="nav-link <?= (Session::get('admMnActive') == 'wisata') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-map-marked-alt"></i>
              <p>
                Wisata
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/testimoni" class="nav-link <?= (Session::get('admMnActive') == 'testimoni') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-thumbs-up"></i>
              <p>
                Testimoni
              </p>
            </a>
          </li>
          <li class="nav-item <?= (Session::get('admMnActive') == 'pengaturan' || Session::get('admMnActive') == 'slider') ? 'menu-open' : '' ?>">
            <a href="#" class="nav-link <?= (Session::get('admMnActive') == 'pengaturan' || Session::get('admMnActive') == 'slider') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Pengaturan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/admin/pengaturan" class="nav-link <?= (Session::get('admMnActive') == 'pengaturan') ? 'active' : '' ?>">
                  <i class="fas fa-desktop nav-icon"></i>
                  <p>Pengaturan Website</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/admin/slider" class="nav-link <?= (Session::get('admMnActive') == 'slider') ? 'active' : '' ?>">
                  <i class="fas fa-layer-group nav-icon"></i>
                  <p>Slider</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header">Penyewaan</li>
          <li class="nav-item">
            <a href="javascript:void(0)" class="nav-link <?= (Session::get('admMnActive') == 'peralatan') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-campground"></i>
              <p>
                Peralatan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/pemesanan" class="nav-link <?= (Session::get('admMnActive') == 'pemesanan') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Pemesanan
              </p>
            </a>
          </li>
          <li class="nav-header">Pengaturan Akun</li>
          <li class="nav-item">
            <a href="/admin/akun" class="nav-link <?= (Session::get('admMnActive') == 'akun') ? 'active' : '' ?>">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Akun
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/admin/logout-process" class="nav-link">
              <i class="nav-icon fas fa-power-off"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
