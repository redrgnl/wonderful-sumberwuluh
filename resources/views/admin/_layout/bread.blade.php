<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">{{ $breadcrumb }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            @if ($breadcrumb == 'Dashboard')
                <li class="breadcrumb-item"><a href="#">{{ $breadcrumb }}</a></li>
            @else
                <li class="breadcrumb-item"><a href="/admin/dashboard">Beranda</a></li>
                <li class="breadcrumb-item active">{{ $breadcrumb }}</li>
            @endif
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
