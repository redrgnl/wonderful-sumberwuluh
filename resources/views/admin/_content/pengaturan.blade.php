@extends('admin._layout.index')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="/admin/save-pengaturan" method="POST">
                @csrf
                <input type="hidden" name="inpid" value="{{ $setting->set_id }}">
                <div class="form-group">
                    <label>Link Foto</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpfoto" required value="{{ $setting->set_foto }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Judul</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-book"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpjudul" required value="{{ $setting->set_title }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Deskripsi</label>
                    <textarea style="width: 100%" name="inpdesk"></textarea>
                </div>
                <div class="form-group">
                    <label>Judul Intro</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-list-alt"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpintjudul" required value="{{ $setting->set_intro_title }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Deskripsi Intro</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpintdesk" required value="{{ $setting->set_intro_subtitle }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Thumbnail Intro</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpintthumb" required value="{{ $setting->set_intro_thumbnail }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Video Intro</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-photo-video"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpintvid" required value="{{ $setting->set_intro_video }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Background Intro</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpintbg" required value="{{ $setting->set_intro_foto }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Background Penyewaan</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpswbg" required value="{{ $setting->set_sewa_bg }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Foto Penyewaan</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpswft" required value="{{ $setting->set_sewa_foto}}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Judul Penyewaan</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-list-alt"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpswjd" required value="{{ $setting->set_sewa_title}}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Deskripsi Penyewaan</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpswdesk" required value="{{ $setting->set_sewa_desk}}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Background Footer</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpfotbg" required value="{{ $setting->set_footer_foto }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Deskripsi Footer</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpfotdesk" required value="{{ $setting->set_footer_desk }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Facebook</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-link"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpfb" required value="{{ $setting->set_fb }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Instagram</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-link"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpig" required value="{{ $setting->set_ig }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Twitter</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-link"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inptw" required value="{{ $setting->set_twitter }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Nama CP</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpnmcp" required value="{{ $setting->set_nama }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Alamat CP</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-home"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpalcp" required value="{{ $setting->set_alamat }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Telepon</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-phone"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inptpcp" required value="{{ $setting->set_telp }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Whatsapp</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-whatsapp"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpwacp" required value="{{ $setting->set_wa }}">
                    </div>
                </div>
                <div class="form-grup">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
        var ckdesk = CKEDITOR.replace('inpdesk', {
			language: 'id',
            extraPlugins: 'notification'
        });
        ckdesk.on( 'required', function( evt ) {
            ckdesk.showNotification( 'This field is required.', 'warning' );
            evt.cancel();
        } );
        CKEDITOR.instances['inpdesk'].setData(`<?= $setting->set_desk ?>`)
    });
</script>
@endsection
