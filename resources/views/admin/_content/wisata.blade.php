@extends('admin._layout.index')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Destinasi Wisata</h3>
                <div class="text-right">
                    <button type="button" data-toggle="modal" data-target="#modal-lg-add" class="btn btn-primary"><i class="fas fa-plus"></i> &ensp; Tambah</button>
                </div>
            </div>
            <div class="card-body">
                <table id="data-table" class="table table-striped table-bordered nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>Foto</th>
                            <th>Update</th>
                            <th>Creator</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;?>
                        @foreach ($wisata as $wisata)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $wisata->wis_nama }}</td>
                            <td>{{ substr(strip_tags($wisata->wis_desk), 0, 20) }}</td>
                            <td><img src="{{ $wisata->wis_foto }}" width="100" /></td>
                            <td>{{ date("F d, Y", strtotime($wisata->created_at)) }}</td>
                            <td>{{ ucwords($wisata->adm_nama) }}</td>
                            <td>
                                <button type="button" data-toggle="modal" data-target="#modal-lg-edit" class="btn btn-success" onclick="editData('{{ $wisata->wis_id }}', '{{ $wisata->wis_nama }}', '{{ $wisata->wis_desk }}', '{{ $wisata->wis_foto }}')"><i class="fas fa-cog"></i></button>
                                <button type="button" data-toggle="modal" data-target="#modal-lg-delete" class="btn btn-danger" onclick="deleteData('{{ $wisata->wis_id }}', '{{ $wisata->wis_nama }}')"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>Foto</th>
                            <th>Update</th>
                            <th>Creator</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg-add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="/admin/add-data-wisata" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Testimoni</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Tempat Wisata</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-mountain"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpnama" id="inpnama" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Deskripsi</label>
                    <textarea style="width: 100%" name="inpdesk"></textarea>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpfoto" id="inpfoto" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-success">
            <form action="/admin/edit-data-wisata" method="post">
            @csrf
            <input type="hidden" name="edid" id="edid">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data Slider</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-mountain"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="ednama" id="ednama" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Deskripsi</label>
                    <textarea style="width: 100%" name="eddesk"></textarea>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="edfoto" id="edfoto" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg-delete">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-danger">
            <form action="/admin/delete-data-wisata" method="POST">
            @csrf
            <input type="hidden" id="delid" name="delid">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data ini?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-center mb-4">Apakah Anda Yakin Akan Menghapus Data ini?</h5>
                <h3 id="delnama" class="text-center mb-4"></h3>
                <h5 class="text-center mb-3">Data yang dihapus tidak dapat dikembalikan!</h5>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
        var ckdesk = CKEDITOR.replace('inpdesk', {
			language: 'id',
            extraPlugins: 'notification'
        });
        ckdesk.on( 'required', function( evt ) {
            ckdesk.showNotification( 'This field is required.', 'warning' );
            evt.cancel();
        });

        var edckdesk = CKEDITOR.replace('eddesk', {
			language: 'id',
            extraPlugins: 'notification'
        });
        edckdesk.on( 'required', function( evt ) {
            edckdesk.showNotification( 'This field is required.', 'warning' );
            evt.cancel();
        });
    });

    function editData(id, nama, desk, foto) {
        $('#edid').val(id)
        $('#ednama').val(nama)
        $('#edfoto').val(foto)
        CKEDITOR.instances['eddesk'].setData(desk)
    }

    function deleteData(id, nama) {
        $('#delid').val(id)
        $('#delnama').html(nama)
    }
</script>
@endsection
