@extends('admin._layout.index')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="/admin/save-akun" method="POST">
                @csrf
                <input type="hidden" name="inpid" value="{{ $akun->adm_id }}">
                <div class="form-group">
                    <label>Username</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpnama" required value="{{ $akun->adm_nama }}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inppass" required placeholder="* * * * * *">
                    </div>
                </div>

                <div class="form-grup">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
