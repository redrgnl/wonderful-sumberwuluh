@extends('admin._layout.index')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Slider</h3>
                <div class="text-right">
                    <button type="button" data-toggle="modal" data-target="#modal-lg-add" class="btn btn-primary"><i class="fas fa-plus"></i> &ensp; Tambah</button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="data-table" class="table table-striped table-bordered nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Title</th>
                                    <th>Subtitle</th>
                                    <th>Foto</th>
                                    <th>Update</th>
                                    <th>Creator</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;?>
                                @foreach ($slider as $slider)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ substr($slider->sli_title, 0, 20) }}</td>
                                    <td>{{ substr($slider->sli_subtitle, 0, 20) }}</td>
                                    <td><img src="{{ $slider->sli_foto }}" width="100" /></td>
                                    <td>{{ date("F d, Y", strtotime($slider->created_at)) }}</td>
                                    <td>{{ ucwords($slider->adm_nama) }}</td>
                                    <td>
                                        <button type="button" data-toggle="modal" data-target="#modal-lg-edit" class="btn btn-success" onclick="editData('{{ $slider->sli_id }}', '{{ $slider->sli_title }}', '{{ $slider->sli_subtitle }}', '{{ $slider->sli_foto }}')"><i class="fas fa-cog"></i></button>
                                        <button type="button" data-toggle="modal" data-target="#modal-lg-delete" class="btn btn-danger" onclick="deleteData('{{ $slider->sli_id }}', '{{ $slider->sli_title }}', '{{ $slider->sli_subtitle }}')"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No.</th>
                                    <th>Title</th>
                                    <th>Subtitle</th>
                                    <th>Foto</th>
                                    <th>Update</th>
                                    <th>Creator</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg-add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="/admin/add-data-slider" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Slider</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Title</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inptitle" id="inptitle" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Subtitle</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpsubtitle" id="inpsubtitle" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Foto</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpfoto" id="inpfoto" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-success">
            <form action="/admin/edit-data-slider" method="post">
            @csrf
            <input type="hidden" name="edid" id="edid">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data Slider</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Title</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="edtitle" id="edtitle" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Subtitle</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="edsubtitle" id="edsubtitle" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Link Foto</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="edfoto" id="edfoto" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg-delete">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-danger">
            <form action="/admin/delete-data-slider" method="POST">
            @csrf
            <input type="hidden" id="delid" name="delid">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data ini?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-center mb-4">Apakah Anda Yakin Akan Menghapus Data ini?</h5>
                <h3 id="deltitle" class="text-center mb-2">uwu</h3>
                <h4 id="delsubtitle" class="text-center mb-4">uwu</h4>
                <h5 class="text-center mb-3">Data yang dihapus tidak dapat dikembalikan!</h5>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function editData(id, title, subtitle, foto) {
        $('#edid').val(id)
        $('#edtitle').val(title)
        $('#edsubtitle').val(subtitle)
        $('#edfoto').val(foto)
    }

    function deleteData(id, title, subtitle) {
        $('#delid').val(id)
        $('#deltitle').html(title)
        $('#delsubtitle').html(subtitle)
    }
</script>
@endsection
