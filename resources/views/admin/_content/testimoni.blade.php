@extends('admin._layout.index')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Testimoni Wisatawan</h3>
                <div class="text-right">
                    <button type="button" data-toggle="modal" data-target="#modal-lg-add" class="btn btn-primary"><i class="fas fa-plus"></i> &ensp; Tambah</button>
                </div>
            </div>
            <div class="card-body">
                <table id="data-table" class="table table-striped table-bordered nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>Foto</th>
                            <th>Destinasi</th>
                            <th>Update</th>
                            <th>Creator</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;?>
                        @foreach ($testimoni as $testimoni)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $testimoni->wita_nama }}</td>
                            <td>{{ substr($testimoni->wita_desk, 0, 20) }}</td>
                            <td><img src="{{ $testimoni->wita_foto }}" width="100" /></td>
                            <td>{{ $testimoni->wis_nama }}</td>
                            <td>{{ date("F d, Y", strtotime($testimoni->created_at)) }}</td>
                            <td>{{ ucwords($testimoni->adm_nama) }}</td>
                            <td>
                                <button type="button" data-toggle="modal" data-target="#modal-lg-edit" class="btn btn-success" onclick="editData('{{ $testimoni->wita_id }}', '{{ $testimoni->wita_nama }}', '{{ $testimoni->wita_desk }}', '{{ $testimoni->wita_foto }}', '{{ $testimoni->wis_id }}')"><i class="fas fa-cog"></i></button>
                                <button type="button" data-toggle="modal" data-target="#modal-lg-delete" class="btn btn-danger" onclick="deleteData('{{ $testimoni->wita_id }}', '{{ $testimoni->wita_nama }}', '{{ $testimoni->wita_desk }}')"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>Foto</th>
                            <th>Destinasi</th>
                            <th>Update</th>
                            <th>Creator</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg-add">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="/admin/add-data-testimoni" method="post">
            @csrf
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Testimoni</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpnama" id="inpnama" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Deskripsi</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="inpdesk" id="inpdesk" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="inpfoto" id="inpfoto" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-mountain"></i></span>
                        </div>
                        <select class="custom-select" required name="inpwisata" id="inpwisata">
                            <option value="">- Pilih Tempat Wisata -</option>
                            @foreach ($wisata as $w)
                            <option value="{{ $w->wis_id }}">{{ $w->wis_nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-success">
            <form action="/admin/edit-data-testimoni" method="post">
            @csrf
            <input type="hidden" name="edid" id="edid">
            <div class="modal-header">
                <h4 class="modal-title">Edit Data Slider</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="ednama" id="ednama" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Deskripsi</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-bars"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^':\x22]*$" name="eddesk" id="eddesk" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-image"></i></span>
                        </div>
                        <input type="text" class="form-control" autocomplete="off" pattern="[^'\x22]*$" name="edfoto" id="edfoto" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-mountain"></i></span>
                        </div>
                        <select class="custom-select" required name="edwisata" id="edwisata">
                            <option value="">- Pilih Tempat Wisata -</option>
                            @foreach ($wisata as $w)
                            <option value="{{ $w->wis_id }}">{{ $w->wis_nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-lg-delete">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-danger">
            <form action="/admin/delete-data-testimoni" method="POST">
            @csrf
            <input type="hidden" id="delid" name="delid">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data ini?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-center mb-4">Apakah Anda Yakin Akan Menghapus Data ini?</h5>
                <h3 id="delnama" class="text-center mb-2"></h3>
                <h4 id="deldesk" class="text-center mb-4"></h4>
                <h5 class="text-center mb-3">Data yang dihapus tidak dapat dikembalikan!</h5>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-default">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function editData(id, nama, desk, foto, wisata) {
        $('#edid').val(id)
        $('#ednama').val(nama)
        $('#eddesk').val(desk)
        $('#edfoto').val(foto)
        $('#edwisata').val(wisata)
    }
    function deleteData(id, nama, desk) {
        $('#delid').val(id)
        $('#delnama').html(nama)
        $('#deldesk').html(desk)
    }
</script>
@endsection
