@extends('admin._layout.index')

@section('content')
<div class="row">
    <div class="col-lg-3 col-6">
      <div class="small-box bg-info">
        <div class="inner">
          <h3><i class="fas fa-desktop"></i></h3>
          <p>Pengaturan Website</p>
        </div>
        <div class="icon">
          <i class="fas fa-desktop"></i>
        </div>
        <a href="/admin/dashboard" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-6">
      <div class="small-box bg-success">
        <div class="inner">
          <h3>{{ $wisata }}</h3>
          <p>Tempat Wisata</p>
        </div>
        <div class="icon">
          <i class="fas fa-map-marked-alt"></i>
        </div>
        <a href="/admin/wisata" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-6">
      <div class="small-box bg-warning">
        <div class="inner">
          <h3>{{ $wisatawan }}</h3>
          <p>Testimoni</p>
        </div>
        <div class="icon">
          <i class="fas fa-thumbs-up"></i>
        </div>
        <a href="/admin/testimoni" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-6">
      <div class="small-box bg-danger">
        <div class="inner">
          <h3>{{ $slider }}</h3>
          <p>Jumlah Background Slider</p>
        </div>
        <div class="icon">
          <i class="fas fa-layer-group"></i>
        </div>
        <a href="/admin/pengaturan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
</div>
@endsection

@section('script')

@endsection
