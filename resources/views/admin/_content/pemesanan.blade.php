@extends('admin._layout.index')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Data Penyewaan Alat</h3>
            </div>
            <div class="card-body">
                <table id="data-table" class="table table-striped table-bordered nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>ID</th>
                            <th>Konfirmasi</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Telepon / WA</th>
                            <th>Tanggal</th>
                            <th>Alat</th>
                            <th>Total</th>
                            <th>Status Pengembalian</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1; $total = 0; ?>
                        @foreach ($pemesanan as $pms)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>
                                {{ $pms->book_id }}
                                @if ($pms->book_status != 0)
                                    <span class="badge badge-success">Sudah Dibayar</span>
                                    @if ($pms->book_kembali != 0)
                                        <span class="badge badge-primary">Sudah Dikembalikan</span>
                                    @else
                                        <span class="badge badge-danger">Belum Dikembalikan</span>
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if ($pms->book_status == 0)
                                    <a href="/admin/pemesanan/konfirmasi/{{ $pms->book_id }}" class="btn btn-success"><i class="fas fa-check"></i></a>
                                @else
                                    <a href="/admin/pemesanan/batal/{{ $pms->book_id }}" class="btn btn-danger"><i class="fas fa-exclamation"></i></a>
                                @endif
                            </td>
                            <td>{{ $pms->book_nama }}</td>
                            <td>{{ $pms->book_alamat }}</td>
                            <td>{{ $pms->book_telp }}</td>
                            <td>
                                <p>{{ $pms->book_start }}</p>
                                <p>Sampai</p>
                                <p>{{ $pms->book_end }}</p>
                            </td>
                            <td>
                            @foreach ($tenda as $td)
                                @if (in_array($td->tent_id, explode(',', $pms->book_tools)))
                                    <p>{{ $td->tent_nama }}</p>
                                    @if ($pms->book_paket == 0)
                                        <?php $total += ($td->tent_harga*date('d', (strtotime($pms->book_end)-strtotime($pms->book_start))))?>
                                    @endif
                                @endif
                            @endforeach
                            @if ($pms->book_paket != 0)
                                @foreach ($paket as $pk)
                                    @if ($pk->paket_id == $pms->book_paket)
                                        <?php $total += ($pk->paket_harga*date('d', (strtotime($pms->book_end)-strtotime($pms->book_start))))?>
                                    @endif
                                @endforeach
                            @endif
                            </td>
                            <td>Rp. {{ number_format($total, 0, ',', '.') }}</td>
                            <td>
                                @if ($pms->book_status != 0)
                                    @if ($pms->book_kembali == 0)
                                        <a href="/admin/pemesanan/dikembalikan/{{ $pms->book_id }}" class="btn btn-success"><i class="fas fa-redo"></i></a>
                                    @else
                                        <a href="/admin/pemesanan/belum-dikembalikan/{{ $pms->book_id }}" class="btn btn-danger"><i class="fas fa-exclamation"></i></a>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <?php $total = 0; ?>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>ID</th>
                            <th>Konfirmasi</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Telepon / WA</th>
                            <th>Tanggal</th>
                            <th>Alat</th>
                            <th>Total</th>
                            <th>Status Pengembalian</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
