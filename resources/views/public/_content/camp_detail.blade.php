@extends('public._layout.index')

@section('content')

<div id="colorlib-blog" style="padding-top: 50px; padding-bottom: 0px">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="wrap-division">
					<article class="animate-box">
						<div class="blog-img" style="background-image: url({{ $tendaDetail->tent_foto }});"></div>
						<div class="desc">
                            <div class="meta">
                                <p>
                                    <span>{{ date("F d, Y", strtotime($tendaDetail->created_at)) }} </span>
                                    <span>{{ ucwords($tendaDetail->adm_nama) }} </span>
                                    <span><b>Rp. {{ $tendaDetail->tent_harga }}</b> per Hari </span>
                                </p>
                            </div>
                            <h2><a>{{ ucwords($tendaDetail->tent_nama) }}</a></h2>
                            <h4 class="text-center">Form Penyewaan Tenda</h4>
                            <form action="#">
                                <div class="row form-group">
                                    <div class="col-md-12 padding-bottom">
                                        <label for="fname">First Name</label>
                                        <input type="text" id="fname" class="form-control" placeholder="Your firstname">
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <input type="submit" value="Send Message" class="btn btn-primary">
                                </div>

                            </form>
						</div>
					</article>
				</div>
			</div>
		</div>
    </div>
</div>

<div id="colorlib-hotel" style="padding-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box fadeInUp animated-fast">
                <h2>Tenda Lainnya</h2>
                <p>We love to tell our successful far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 animate-box">
                <div class="owl-carousel">
                    @foreach($bottTenda as $bt)
                    <div class="item">
                        <div class="hotel-entry">
                            <a href="/camp-tools/{{ str_replace(' ', '-', strtolower($bt->tent_nama)) }}" class="hotel-img" style="background-image: url({{ $bt->tent_foto }});"></a>
                            <div class="desc">
                                <h2 class="truncate-text"><a href="/camp-tools/{{ str_replace(' ', '-', strtolower($bt->tent_nama)) }}">{{ ucwords($bt->tent_nama) }}</a></h2>
                                <p class="city truncate-text"><b>{{ $bt->tent_kapasitas }}</b> Orang &ensp;
                                    @if($bt->tent_sedia == 1)
                                        <label class="label-green">Tersedia</label>
                                    @else
                                        <label class="label-red">Tidak Tersedia</label>
                                    @endif
                                </p>
                                <p class="city"><b>Rp. {{ number_format($bt->tent_harga, 0, ',', '.') }}</b> / Hari</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
