@extends('public._layout.index')

@section('content')

<div id="colorlib-blog" style="padding-top: 50px; padding-bottom: 0px">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="wrap-division">
					<article class="animate-box">
						<div class="blog-img" style="background-image: url({{ $destinationDetail->wis_foto }});"></div>
						<div class="desc">
                            <div class="meta">
                                <p>
                                    <span>{{ date("F d, Y", strtotime($destinationDetail->created_at)) }} </span>
                                    <span>{{ ucwords($destinationDetail->adm_nama) }} </span>
                                </p>
                            </div>
                            <h2><a>{{ ucwords($destinationDetail->wis_nama) }}</a></h2>
                            <?= $destinationDetail->wis_desk ?>
						</div>
					</article>
				</div>
			</div>
		</div>
    </div>
    @if($bottWisatawan->count() != 0)
    <div class="container">
        <h2>Galeri Pengunjung</h2>
        <hr>
        <div class="row">
            <div class="col-md-12 animate-box">
                <div class="owl-carousel">
                    @foreach($bottWisatawan as $bw)
                    <div class="item">
                        <div class="hotel-entry">
                            <a href="/destination/{{ str_replace(' ', '-', strtolower($bw->wis_nama)) }}" class="hotel-img" style="background-image: url({{ $bw->wita_foto }});"></a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif
</div>

<div id="colorlib-hotel" style="padding-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box fadeInUp animated-fast">
                <h2>Destinasi Wisata Lainnya</h2>
                <p>We love to tell our successful far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 animate-box">
                <div class="owl-carousel">
                    @foreach($bottDestination as $bd)
                    <div class="item">
                        <div class="hotel-entry">
                            <a href="/destination/{{ str_replace(' ', '-', strtolower($bd->wis_nama)) }}" class="hotel-img" style="background-image: url({{ $bd->wis_foto }});"></a>
                            <div class="desc">
                                <h3 class="truncate-text"><a href="/destination/{{ str_replace(' ', '-', strtolower($bd->wis_nama)) }}">{{ ucwords($bd->wis_nama) }}</a></h3>
                                <span class="place truncate-text">{{ strip_tags($bd->wis_desk) }}</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
