@extends('public._layout.index')

@section('content')

<div id="colorlib-hotel">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box fadeInUp animated-fast">
                <h2>Form Booking Peralatan Camping</h2>
                <p>Masukkan identitas anda untuk melanjutkan proses pemesanan peralatan camping</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 animate-box">
                <form action="/camp-tools/save-book" method="POST">
                    @csrf
                    <button type="submit" id="btn-submit" class="hidden"></button>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inpnama">Nama</label>
                            <input type="text" name="inpnama" id="inpnama" class="form-control text-black-bold" placeholder="Masukkan Nama Anda" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inpnik">NIK</label>
                            <input type="text" name="inpnik" id="inpnik" class="form-control text-black-bold" placeholder="Masukkan Nomor NIK KTP Anda" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inpalamat">Alamat</label>
                            <input type="text" name="inpalamat" id="inpalamat" class="form-control text-black-bold" placeholder="Masukkan Alamat Lengkap Anda" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inptelp">No Telepon / WA</label>
                            <input type="text" name="inptelp" id="inptelp" class="form-control text-black-bold" placeholder="Masukkan Nomor Telpon / WA Anda" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inpemail">Email</label>
                            <input type="text" name="inpemail" id="inpemail" class="form-control text-black-bold" placeholder="Masukkan Email Anda">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 padding-bottom">
                            <label for="inpstart">Tanggal Mulai</label>
                            <input type="date" name="inpstart" id="inpstart" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="inpend">Tanggal Selesai</label>
                            <input type="date" name="inpend" id="inpend" class="form-control" required>
                        </div>
                    </div>
                    <h3>Daftar Perlengkapan Camping</h3>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="wrap-division">
                                <?php $chno = 1; ?>
                                @foreach ($tenda as $td)
                                <div class="col-md-3 col-xs-6 animate-box">
                                    <div class="tour">
                                        <a href="javascript:void(0)" onclick="checkbutton('#check_{{ $chno }}')" class="tour-img" style="background-image: url({{ $td->tent_foto }});"></a>
                                        <span class="desc">
                                            <h2 class="truncate-text" onclick="checkbutton('#check_{{ $chno }}')">
                                                <input type="checkbox" style="width: 16px; height: 16px" name="checktools[]" value="{{ $td->tent_id }}" id="check_{{ $chno }}" onclick="checkbutton('#check_{{ $chno }}')">
                                                <a>{{ ucwords($td->tent_nama) }}</a>
                                            </h2>
                                            <p class="city truncate-text">
                                                @if($td->tent_kapasitas != 0)
                                                    <b>{{ $td->tent_kapasitas }}</b> Orang &ensp;
                                                @endif
                                                @if($td->tent_sedia == 1)
                                                    <label class="label-green">Tersedia</label>
                                                @else
                                                    <label class="label-red">Tidak Tersedia</label>
                                                @endif
                                            </p>
                                            <p class="city"><b>Rp. {{ number_format($td->tent_harga, 0, ',', '.') }}</b> / Hari</p>
                                        </span>
                                    </div>
                                </div>
                                <?php $chno++; ?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button id="btn-confirmation" type="button" class="btn btn-primary">Booking Sekarang</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('#btn-confirmation').on('click', function() {
        if ($('#inpnama').val() == '' || $('#inpnik').val() == '' || $('#inpalamat').val() == '' || $('#inptelp').val() == '' || $('#inpstart').val() == '' || $('#inpend').val() == '') {
            $('#btn-submit').click()
        } else {
            var start = $('#inpstart').val();
            var end = $('#inpend').val();
            if (new Date(start).getTime() > new Date(end).getTime()) {
                $('#inpend').val('');
                $('#btn-submit').click()
            } else {
                $('#btn-submit').click()
            }
        }
    })

    function checkbutton(id) {
        if($(id).is(':checked')) {
            $(id).prop('checked', false)
        } else {
            $(id).prop('checked', true)
        }
    }
</script>
@endsection
