@extends('public._layout.index')

@section('content')

<div class="colorlib-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box" style="margin-bottom: 20px">
                <h2>Paket Camping</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="wrap-division">
                        @foreach ($paket as $pk)
                        <div class="col-md-3 col-xs-6 animate-box">
                            <div class="tour">
                                <a href="/camp-tools/paket/{{ str_replace(' ', '-', strtolower($pk->paket_nama)) }}" class="tour-img" style="background-image: url({{ $pk->paket_foto }});"></a>
                                <span class="desc">
                                    <h2 class="truncate-text"><a href="/camp-tools/paket/{{ str_replace(' ', '-', strtolower($pk->paket_nama)) }}">{{ ucwords($pk->paket_nama) }}</a></h2>
                                    <p class="city truncate-text">
                                        <a href="/camp-tools/paket/{{ str_replace(' ', '-', strtolower($pk->paket_nama)) }}" class="btn btn-primary">Lihat Paket</a>
                                    </p>
                                    <p class="city"><b>Rp. {{ number_format($pk->paket_harga, 0, ',', '.') }}</b> / Hari</p>
                                </span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box" style="margin-bottom: 20px">
                <h2>Peralatan Camping</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="wrap-division">
                        @foreach ($tenda as $td)
                        <div class="col-md-3 col-xs-6 animate-box">
                            <div class="tour">
                                <a class="tour-img" style="background-image: url({{ $td->tent_foto }});"></a>
                                <span class="desc">
                                    <h2 class="truncate-text"><a>{{ ucwords($td->tent_nama) }}</a></h2>
                                    <p class="city truncate-text">
                                        @if($td->tent_kapasitas != 0)
                                            <b>{{ $td->tent_kapasitas }}</b> Orang &ensp;
                                        @endif
                                        @if($td->tent_sedia == 1)
                                            <label class="label-green">Tersedia</label>
                                        @else
                                            <label class="label-red">Tidak Tersedia</label>
                                        @endif
                                    </p>
                                    <p class="city"><b>Rp. {{ number_format($td->tent_harga, 0, ',', '.') }}</b> / Hari</p>
                                </span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <a href="/camp-tools/book" class="btn btn-primary">Booking Sekarang</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection
