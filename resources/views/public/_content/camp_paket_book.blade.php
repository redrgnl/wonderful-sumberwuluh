@extends('public._layout.index')

@section('content')

<div id="colorlib-hotel">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box fadeInUp animated-fast">
                <h2>Form Booking Peralatan Camping</h2>
                <p>Masukkan identitas anda untuk melanjutkan proses pemesanan peralatan camping</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 animate-box">
                <form action="/camp-tools/save-book" method="POST">
                    @csrf
                    <input type="hidden" name="inppaket" value="{{ $paketDetail->paket_id }}">
                    <input type="hidden" name="inppakettools" value="{{ $paketDetail->paket_tools }}">
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inpnama">Nama</label>
                            <input type="text" name="inpnama" id="inpnama" class="form-control text-black-bold" placeholder="Masukkan Nama Anda" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inpnik">NIK</label>
                            <input type="text" name="inpnik" id="inpnik" class="form-control text-black-bold" placeholder="Masukkan Nomor NIK KTP Anda" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inpalamat">Alamat</label>
                            <input type="text" name="inpalamat" id="inpalamat" class="form-control text-black-bold" placeholder="Masukkan Alamat Lengkap Anda" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inptelp">No Telepon / WA</label>
                            <input type="text" name="inptelp" id="inptelp" class="form-control text-black-bold" placeholder="Masukkan Nomor Telpon / WA Anda" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="inpemail">Email</label>
                            <input type="text" name="inpemail" id="inpemail" class="form-control text-black-bold" placeholder="Masukkan Email Anda">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 padding-bottom">
                            <label for="inpstart">Tanggal Mulai</label>
                            <input type="date" name="inpstart" id="inpstart" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label for="inpend">Tanggal Selesai</label>
                            <input type="date" name="inpend" id="inpend" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                            <h3>Detail Paket</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="wrap-division">
                                <article class="animate-box">
                                    <div class="blog-img" style="background-image: url({{ $paketDetail->paket_foto }});"></div>
                                    <div class="desc">
                                        <div class="meta">
                                            <p>
                                                <span>{{ date("F d, Y", strtotime($paketDetail->created_at)) }} </span>
                                            </p>
                                        </div>
                                        <h2><a>{{ ucwords($paketDetail->paket_nama) }}</a></h2>
                                        <p><?= $paketDetail->paket_desk ?></p>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 animate-box">
                            <div class="row">
                                <div class="wrap-division">
                                    @foreach ($tenda as $td)
                                        @if (in_array($td->tent_id, explode(',', $paketDetail->paket_tools)))
                                            <div class="col-md-4 col-xs-6 animate-box">
                                                <div class="tour">
                                                    <a class="tour-img" style="background-image: url({{ $td->tent_foto }});"></a>
                                                    <span class="desc">
                                                        <h2 class="truncate-text"><a>{{ ucwords($td->tent_nama) }}</a></h2>
                                                        <p class="city truncate-text">
                                                            @if($td->tent_kapasitas != 0)
                                                                <b>{{ $td->tent_kapasitas }}</b> Orang &ensp;
                                                            @endif
                                                        </p>
                                                    </span>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary">Booking Sekarang</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
