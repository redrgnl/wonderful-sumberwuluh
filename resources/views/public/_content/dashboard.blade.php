@extends('public._layout.index')

@section('content')

<div class="colorlib-tour colorlib-light-grey" style="padding-bottom: 50px;">
    <div class="tour-wrap">
        @foreach ($destinasi as $d)
        <a href="/destination/{{ str_replace(' ', '-', strtolower($d->wis_nama)) }}" class="tour-entry animate-box">
            <div class="tour-img" style="background-image: url({{ $d->wis_foto }});">
            </div>
            <span class="desc">
                <h2 class="truncate-text">{{ ucwords($d->wis_nama) }}</h2>
                <span class="city truncate-text">{{ strip_tags($d->wis_desk) }}</span>
            </span>
        </a>
        @endforeach
    </div>
</div>

<div id="colorlib-blog" style="padding-top: 50px; padding-bottom: 40px">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="wrap-division">
					<article class="animate-box">
						<div class="blog-img" style="background-image: url({{ $setting->set_foto }});"></div>
						<div class="desc">
                            <h2><a>{{ $setting->set_title }}</a></h2>
							<?= $setting->set_desk ?>
						</div>
					</article>
				</div>
			</div>

			<div class="col-md-4" style="padding-top: 20px">
				<div class="sidebar-wrap">
					<div class="side animate-box">
                        <h3 class="sidebar-heading">Destinasi Wisata</h3>
                        @foreach ($destinasi_side as $ds)
                        <div class="blog-entry-side">
							<a href="/destination/{{ str_replace(' ', '-', strtolower($ds->wis_nama)) }}" class="blog-post">
                                <span class="img" style="background-image: url({{ $ds->wis_foto }});"></span>
								<div class="desc">
									<h5 class="truncate-text" style="padding-top: 20px; margin-bottom: 20px;"><b>{{ ucwords($ds->wis_nama) }}</b></h5>
									<span class="cat truncate-text">{{ substr(strip_tags($ds->wis_desk), 0, 40) }}</span>
								</div>
							</a>
                        </div>
                        @endforeach
                        <div class="blog-entry-side">
                            <div class="col-md-12">
                                <a href="/destination" class="btn btn-primary btn-block">Destinasi Lainnya <i class="fas fa-angle-right"></i></a>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="colorlib-intro" class="intro-img" style="background-image: url({{ $setting->set_intro_foto }});" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 animate-box">
                <div class="intro-desc">
                    <div class="text-salebox">
                        <div class="text-rights">
                            <h3 class="title">{{ $setting->set_intro_title }}</h3>
                            <p>{{ $setting->set_intro_subtitle }}</p>
                            <p><a href="/destination" class="btn btn-primary btn-outline">Destinasi Wisata Lainnya</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 animate-box">
                <div class="video-wrap">
                    <div class="video colorlib-video" style="background-image: url({{ $setting->set_intro_thumbnail }});">
                        <a href="{{ $setting->set_intro_video }}" class="popup-vimeo"><i class="icon-video"></i></a>
                        <div class="video-overlay"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="colorlib-hotel">
    <div class="container">
        <div class="row">
            <div class="col-md-12 animate-box">
                <div class="owl-carousel">
                    @foreach ($wisatawan as $ws)
                    <div class="item">
                        <div class="hotel-entry">
                            <a class="hotel-img" style="background-image: url({{ $ws->wita_foto }});"></a>
                            <div class="desc">
                                <h3 class="truncate-text"><a>{{ $ws->wita_nama }}</a></h3>
                                <span class="place truncate-text">{{ strip_tags($ws->wita_desk) }}</span>
                                <p class="truncate-text">{{ $ws->wis_nama }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div id="colorlib-intro" class="intro-img" style="background-image: url({{ $setting->set_sewa_bg }}); margin-bottom: 120px" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 animate-box">
                <div class="video-wrap">
                    <div class="video colorlib-video" style="background-image: url({{ $setting->set_sewa_foto }});"></div>
                </div>
            </div>
            <div class="col-md-6 animate-box" style="margin-top: 20px">
                <div class="intro-desc">
                    <div class="text-salebox">
                        <div class="text-rights">
                            <h3 class="title">{{ $setting->set_sewa_title }}</h3>
                            <p>{{ $setting->set_sewa_desk }}</p>
                            <p><a href="/camp-tools" class="btn btn-primary btn-outline">Pesan Sekarang</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
