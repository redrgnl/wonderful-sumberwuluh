@extends('public._layout.index')

@section('content')

<div id="colorlib-blog" style="padding-top: 50px; padding-bottom: 0px">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="wrap-division">
					<article class="animate-box">
						<div class="blog-img" style="background-image: url({{ $paketDetail->paket_foto }});"></div>
						<div class="desc">
                            <div class="meta">
                                <p>
                                    <span>{{ date("F d, Y", strtotime($paketDetail->created_at)) }} </span>
                                    <span>{{ ucwords($paketDetail->adm_nama) }} </span>
                                </p>
                            </div>
                            <h2><a>{{ ucwords($paketDetail->paket_nama) }}</a></h2>
                            <p><?= $paketDetail->paket_desk ?></p>
                            <br>
                            <a href="/camp-tools/paket/put/{{ $paketDetail->paket_id }}" class="btn btn-primary">Pesan Sekarang</a>
						</div>
					</article>
				</div>
			</div>
		</div>
    </div>
    <div class="container">
        <h2>Detail Peralatan Camping</h2>
        <hr>
        <div class="row">
            <div class="col-md-12 animate-box">
                <div class="row">
                    <div class="wrap-division">
                        @foreach ($bottPeralatan as $bp)
                            @if (in_array($bp->tent_id, explode(',', $paketDetail->paket_tools)))
                                <div class="col-md-2 col-xs-4 animate-box">
                                    <div class="tour">
                                        <a class="tour-img" style="background-image: url({{ $bp->tent_foto }});"></a>
                                        <span class="desc">
                                            <h2 class="truncate-text"><a>{{ ucwords($bp->tent_nama) }}</a></h2>
                                            <p class="city truncate-text">
                                                @if($bp->tent_kapasitas != 0)
                                                    <b>{{ $bp->tent_kapasitas }}</b> Orang &ensp;
                                                @endif
                                            </p>
                                        </span>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="colorlib-hotel" style="padding-top: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box fadeInUp animated-fast">
                <h2>Paket Camping Lainnya</h2>
                <p>We love to tell our successful far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 animate-box">
                <div class="owl-carousel">
                    @foreach($bottPaket as $bk)
                    <div class="item">
                        <div class="hotel-entry">
                            <a href="/camp-tools/paket/{{ str_replace(' ', '-', strtolower($bk->paket_nama)) }}" class="hotel-img" style="background-image: url({{ $bk->paket_foto }});"></a>
                            <div class="desc">
                                <h3 class="truncate-text"><a href="/camp-tools/paket/{{ str_replace(' ', '-', strtolower($bk->paket_nama)) }}">{{ ucwords($bk->paket_nama) }}</a></h3>
                                <span class="place truncate-text"><b>Rp. {{ number_format($bk->paket_harga, 0, ',', '.') }}</b> / Hari</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
