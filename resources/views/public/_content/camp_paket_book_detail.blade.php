@extends('public._layout.index')

@section('content')

<div id="colorlib-hotel">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box fadeInUp animated-fast">
                <h2>Detail Booking Peralatan Camping</h2>
                <p>Masukkan identitas anda untuk melanjutkan proses pemesanan peralatan camping</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-1 animate-box">
                <p><b>Atas Nama</b><p>
                <p><h4><b>{{ ucwords($bookDetail->book_nama) }}</b></h4></p>
                <p><b>{{ $bookDetail->book_alamat }}</b><p>
                <p>{{ $bookDetail->book_email }} | {{ $bookDetail->book_telp }}<p>
            </div>
            <div class="col-md-2 col-xs-6 animate-box">
                <p><b>Invoice #</b><p>
                <p><b>Tanggal Pemesanan #</b><p>
            </div>
            <div class="col-md-2 col-xs-6 animate-box">
                <p>{{ $bookDetail->book_id }}</p>
                <p>{{ date("F d, Y", strtotime($bookDetail->created_at)) }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-xs-12 animate-box">
                <br>
                <p class="text-center">Dari Tanggal : {{ date("F d, Y", strtotime($bookDetail->book_start)) }} - Sampai : {{ date("F d, Y", strtotime($bookDetail->book_end)) }}</p>
                <hr>
                <h4>{{ ucwords($paket->paket_nama) }}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 animate-box">
                <table class="fl-table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Alat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $noTools = 1; $total = 0; ?>
                        @foreach ($tenda as $tenda)
                            @if (in_array($tenda->tent_id, explode(',', $bookDetail->book_tools)))
                            <tr>
                                <td><h5>{{ $noTools++ }}</h5></td>
                                <td><h5>{{ $tenda->tent_nama }}</h5></td>
                            </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
                <hr>
                <h4 class="text-right"><b>Rp. {{ number_format($paket->paket_harga, 0, ',', '.') }} x {{ (int)date('d', (strtotime($bookDetail->book_end)-strtotime($bookDetail->book_start))) }}</b></h4>
                <h4 class="text-right"><b>Total : Rp. {{ number_format($paket->paket_harga*date('d', (strtotime($bookDetail->book_end)-strtotime($bookDetail->book_start))), 0, ',', '.') }}</b></h4>
                <div class="text-center">
                    <a href="https://api.whatsapp.com/send?phone=62<?=substr(Helper::setting('set_wa'), 1);?>&text=Halo%20Min%2C%20Saya%20mau%20konfirmasi%20booking%20peralatan%20camping%2E%20%0D%20{{ $bookDetail->book_id }}%20{{ url('/camp-tools/check')."/".$bookDetail->book_id }}" target="_blank" class="btn btn-primary">Konfirmasi</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
