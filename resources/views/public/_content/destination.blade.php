@extends('public._layout.index')

@section('content')

<div class="colorlib-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box" style="margin-bottom: 20px">
                <h2>Destinasi Wisata</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="wrap-division">
                        @foreach ($destinasi as $d)
                        <div class="col-md-3 col-sm-6 animate-box">
                            <div class="tour">
                                <a href="/destination/{{ str_replace(' ', '-', strtolower($d->wis_nama)) }}" class="tour-img" style="background-image: url({{ $d->wis_foto }});"></a>
                                <span class="desc">

                                    <h2 class="truncate-text"><a href="/destination/{{ str_replace(' ', '-', strtolower($d->wis_nama)) }}">{{ ucwords($d->wis_nama) }}</a></h2>
                                    <span class="city truncate-text">{{ strip_tags($d->wis_desk) }}</span>
                                </span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection
