
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>WonderfulSumberwuluh - <?php if (empty($destinationName)) { echo ucwords(Session::get('mnActive')); } else { echo ucwords($destinationName); } ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="Re" />

    <!-- Facebook and Twitter integration -->
    @if(empty($destinationName))
	<meta property="og:title" content="WonderfulSumberwuluh - "/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content="https://bit.ly/wonderful-sumberwuluh"/>
	<meta property="og:site_name" content="https://redrgnl.github.io/uncore"/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="WonderfulSumberwuluh - " />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="https://bit.ly/wonderful-sumberwuluh" />
    <meta name="twitter:card" content="summary_large_image" />
    @else
    <meta property="og:title" content="WonderfulSumberwuluh - {{ ucwords($destinationName) }}"/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content="https://bit.ly/wonderful-sumberwuluh"/>
	<meta property="og:site_name" content="https://redrgnl.github.io/uncore"/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="WonderfulSumberwuluh - {{ ucwords($destinationName) }}" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="https://bit.ly/wonderful-sumberwuluh" />
    <meta name="twitter:card" content="summary_large_image" />
    @endif

	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    @include('public._layout.head')

	</head>
	<body>

	<div class="colorlib-loader"></div>

    <a title="Pemesanan Alat Camping" href="/camp-tools"><img class="btn-camp" src="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/images/campfire_base.png"></a>

	<div id="page">
		<nav class="colorlib-nav" role="navigation">
			<div class="top-menu">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-2">
							<div id="colorlib-logo"><a href="/">WonderfulSumberwuluh</a></div>
						</div>
						<div class="col-xs-10 text-right menu-1">
							<ul>
                                <li class="<?php if(Session::get('mnActive') == 'beranda') { echo 'active'; }?>"><a href="/">Beranda</a></li>
								<li class="<?php if(Session::get('mnActive') == 'destinasi') { echo 'active'; }?>"><a href="/destination">Destinasi</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
        </nav>

        <!-- slider -->
        @include('public._layout.slider')

        <!-- content -->
		@yield('content')

		@include('public._layout.footer')
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
	</div>

	@include('public._layout.script')
    @yield('script')
	</body>
</html>
<script>
    $('.btn-camp').on('click', function() {
        $('.camp-overlay').removeClass('hidden')
    })
    $('.camp-close-button').on('click', function() {
        $('.camp-overlay').addClass('hidden')
    })
</script>

