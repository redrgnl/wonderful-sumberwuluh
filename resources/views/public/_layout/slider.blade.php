<aside id="colorlib-hero">
    <div class="flexslider">
        <ul class="slides">
            @if (Helper::slider() == '')
            <li style="background-image: url(https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/images/tour-3.jpg);">
                <div class="overlay"></div>
                <div class="container-fluids">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                            <div class="slider-text-inner text-center">
                                <h2>Explore our most tavel agency</h2>
                                <h1>Our Travel Agency</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            @else
            <?= Helper::slider() ?>
            @endif
          </ul>
      </div>
</aside>
