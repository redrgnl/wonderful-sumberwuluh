<footer id="colorlib-footer" style="background-image: url({{ Helper::setting('set_footer_foto') }}); background-size: cover;" data-stellar-background-ratio="0.5" role="contentinfo">
    <div class="overlay"></div>
    <div class="container">
        <div class="row row-pb-md">
            <div class="col-md-6 colorlib-widget">
                <h4>Wonderful Sumberwuluh</h4>
                <p>{{ Helper::setting('set_footer_desk') }}</p>
                <p>
                    <ul class="colorlib-social-icons">
                        @if (Helper::setting('set_ig') != '')
                        <li><a target="_blank" href="{{ Helper::setting('set_ig') }}"><i class="icon-instagram"></i></a></li>
                        @endif
                        @if (Helper::setting('set_fb') != '')
                        <li><a target="_blank" href="{{ Helper::setting('set_fb') }}"><i class="icon-facebook"></i></a></li>
                        @endif
                        @if (Helper::setting('set_twitter') != '')
                        <li><a target="_blank" href="{{ Helper::setting('set_twitter') }}"><i class="icon-twitter"></i></a></li>
                        @endif
                    </ul>
                </p>
            </div>
            <div class="col-md-3">
                <h4>Destinasi Wisata</h4>
                <ul class="colorlib-footer-links">
                    <?= Helper::helpWisata(5) ?>
                </ul>
            </div>

            <div class="col-md-3">
                <h4>Contact Information</h4>
                <ul class="colorlib-footer-links">
                    <li>{{ Helper::setting('set_nama') }}</li>
                    <li>{{ Helper::setting('set_alamat') }}</li>
                    <li><a href="tel://{{ Helper::setting('set_telp') }}">{{ Helper::setting('set_telp') }}</a></li>
                    <li><a href="tel://{{ Helper::setting('set_wa') }}">{{ Helper::setting('set_wa') }}</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
