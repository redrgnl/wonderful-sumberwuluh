<!-- Animate.css -->
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/css/icomoon.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/css/bootstrap.css">

<!-- Magnific Popup -->
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/css/magnific-popup.css">

<!-- Flexslider  -->
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/css/flexslider.css">

<!-- Owl Carousel -->
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/css/owl.carousel.min.css">
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/css/owl.theme.default.min.css">

<!-- Date Picker -->
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/css/bootstrap-datepicker.css">
<!-- Flaticons  -->
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/fonts/flaticon/font/flaticon.css">

<!-- Theme style  -->
<link rel="stylesheet" href="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/css/style.css">

<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset('custom/custom.css') }}">

<!-- Modernizr JS -->
<script src="https://redrgnl.github.io/asset/wonderfulsumberwuluh/public/js/modernizr-2.6.2.min.js"></script>

<style>
    .truncate-text {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
</style>
