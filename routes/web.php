<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//admin
Route::get('/admin/login', 'AdminController@login');
Route::post('/admin/login-process', 'AdminController@post_login');
Route::get('/admin/logout-process', 'AdminController@post_logout');

Route::group(['middleware' => 'usersession'], function () {
    Route::get('/admin/dashboard', 'AdminController@dashboard');

    Route::get('/admin/wisata', 'AdminController@wisata');
    Route::post('/admin/add-data-wisata', 'AdminController@addWisata');
    Route::post('/admin/edit-data-wisata', 'AdminController@editWisata');
    Route::post('/admin/delete-data-wisata', 'AdminController@deleteWisata');

    Route::get('/admin/testimoni', 'AdminController@testimoni');
    Route::post('/admin/add-data-testimoni', 'AdminController@addTestimoni');
    Route::post('/admin/edit-data-testimoni', 'AdminController@editTestimoni');
    Route::post('/admin/delete-data-testimoni', 'AdminController@deleteTestimoni');

    Route::get('/admin/pengaturan', 'AdminController@pengaturan');
    Route::post('/admin/save-pengaturan', 'AdminController@savePengaturan');

    Route::get('/admin/slider', 'AdminController@slider');
    Route::post('/admin/add-data-slider', 'AdminController@addSlider');
    Route::post('/admin/edit-data-slider', 'AdminController@editSlider');
    Route::post('/admin/delete-data-slider', 'AdminController@deleteSlider');

    Route::get('/admin/peralatan', 'AdminController@peralatan');
    Route::get('/admin/pemesanan', 'AdminController@pemesanan');
    Route::get('/admin/pemesanan/konfirmasi/{id}', 'AdminController@pemesananKonfirmasi');
    Route::get('/admin/pemesanan/batal/{id}', 'AdminController@pemesananBatal');
    Route::get('/admin/pemesanan/dikembalikan/{id}', 'AdminController@pemesananKembali');
    Route::get('/admin/pemesanan/belum-dikembalikan/{id}', 'AdminController@pemesananBelumKembali');

    Route::get('/admin/akun', 'AdminController@akun');
    Route::post('/admin/save-akun', 'AdminController@saveAkun');
});

//route public
Route::get('/', 'PublicController@index');
Route::get('/destination', 'PublicController@destination');
Route::get('/destination/{name}', 'PublicController@destinationDetail');
Route::get('/camp-tools', 'PublicController@camp');
Route::get('/camp-tools/book', 'PublicController@campBook');
Route::post('/camp-tools/save-book', 'PublicController@campBookSave');
Route::get('/camp-tools/check/{id}', 'PublicController@campBookCheck');
Route::get('/camp-tools/detail', 'PublicController@campBookDetail');

Route::get('/camp-tools/paket/{name}', 'PublicController@campPaketDetail');
Route::get('/camp-tools/paket/put/{id}', 'PublicController@campPaketId');
Route::get('/camp-tools/paket-book', 'PublicController@campPaketBook');
Route::get('/camp-tools/paket-detail', 'PublicController@campPaketBookDetail');

Route::get('/page-not-found', function () {
    return view('404_not_found');
});


